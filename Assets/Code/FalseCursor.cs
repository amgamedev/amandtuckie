﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalseCursor : MonoBehaviour {
    public float zPosition = -9.2f;
    Vector3 temp;
    private void Update()
    {
        Cursor.visible = false;
        temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        temp.z = zPosition;
        transform.position = temp;
    }
}
