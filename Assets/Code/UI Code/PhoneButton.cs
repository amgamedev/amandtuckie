﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneButton : MonoBehaviour {
    public Sprite Regular, Highlighted;

    SpriteRenderer rend;
    private void Start()
    {
        rend = gameObject.GetComponent<SpriteRenderer>();
    }


    private void OnMouseEnter()
    {
        rend.sprite = Highlighted;
    }
    private void OnMouseOver()
    {
        
    }
    private void OnMouseExit()
    {
        rend.sprite = Regular;
    }
}
