﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneController : MonoBehaviour {
    [SerializeField] AnimationCurve MoveDown, MoveIn, moveInRot;
    [SerializeField] float MoveDownDuration, MoveInDuration;

    public Sprite Open, Closed;

    public Transform OutsideAnchor;

    [SerializeField]float closedperiod, closedmagnitude;
    float tim;
    [SerializeField] float openperiod, openmagnitude;
    Vector3 origin, temp;
    
    SpriteRenderer rnd;

    public GameObject buttonsHolder;

    private void Start()
    {
        rnd = transform.GetComponent<SpriteRenderer>();
        
        origin = transform.position;
    }

    bool isOpen, transitioning;

    private void Update()
    {
        if (!transitioning)
        {
            if (!isOpen)
            {
                buttonsHolder.SetActive(false);
                
                temp = origin;
                temp.y += Mathf.Sin(Time.time / closedperiod) * closedmagnitude;
                transform.position = temp;
            }
            else
            {
                buttonsHolder.SetActive(true);
                temp = origin;
                temp.y += Mathf.Sin(Time.time / openperiod) * openmagnitude;
                transform.position = temp;
            }
        }
    }

    float targettime;
    float pcent;
    Vector3 rot;
    IEnumerator transition()
    {
        transitioning = true;
        targettime = Time.time + MoveDownDuration;
        while(Time.time < targettime)
        {
            pcent = 1 - ((targettime - Time.time) / MoveDownDuration);
            pcent = MoveDown.Evaluate(pcent);
            transform.localPosition = ( pcent) * (OutsideAnchor.localPosition);
            yield return null;
        }

        targettime = Time.time + MoveInDuration;
        rot = transform.localEulerAngles;
        rot.z = 90;
        transform.localEulerAngles = rot;
        rnd.sprite = Open;

        buttonsHolder.SetActive(true);
        while (Time.time < targettime)
        {
            pcent = ((targettime - Time.time) / MoveInDuration);
            transform.localPosition = (MoveIn.Evaluate(pcent)) * (OutsideAnchor.localPosition);
            pcent = 1 - pcent;
            pcent = moveInRot.Evaluate(pcent);
            rot.z = pcent * -90;
            rot.z += 90;
            transform.localEulerAngles = rot;
            yield return null;
        }
        transform.localEulerAngles = new Vector3(); 
        transitioning = false;
        isOpen = true;
    }

    private void OnMouseOver()
    {
        if (!isOpen && !transitioning)
        {
            if (Input.GetMouseButtonDown(0))
            {
                transitioning = true;
                Debug.Log("fuuuck");
                StartCoroutine(transition());
            }
        }
    }
}
