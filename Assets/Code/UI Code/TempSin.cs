﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempSin : MonoBehaviour {
    public float period, otherone, more;

    float tim;

    Vector3 origin, temp;
    RectTransform rct;
    private void Start()
    {
        rct = transform.GetComponent<RectTransform>();
        origin = rct.anchoredPosition;
    }


    private void Update()
    {
        temp = origin;
        temp.y += Mathf.Sin(Time.time / period) * otherone;
        rct.anchoredPosition = temp;
    }


}
