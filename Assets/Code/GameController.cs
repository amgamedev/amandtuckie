﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public static GameController instance;
    [SerializeField] float UpdateTimer = 0.4f;

    public Player_Controller playa;

    private void Awake()
    {
        instance = this;
        StartCoroutine(WorldTimer());
    }

    IEnumerator WorldTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(UpdateTimer);
            if(playa.CurrentMonster != null)
            {
                playa.CurrentMonster.DoUpdate();
                
            }
            //updateWorld
        }
    }
}
