﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
    public string Nickname;
    public MonsterStats Statistics;
    
    int[] curupdates = new int[6];

    public void DoUpdate()
    {
        for(int m =0; m < 6; m++)
        {
            curupdates[m]++;
            if(curupdates[m] >= Statistics.statdat[m].cyclesToUpdate)
            {
                Debug.Log("I'm doing it");
                curupdates[m] %= Statistics.statdat[m].cyclesToUpdate;
                Statistics.UpdateStat(m, Statistics.statdat[m].updateValue);
            }
        }
    }
    [System.Serializable]
    public class MonsterStats
    {
        public Stat
            Health,
            Moxie,
            Viscocity,
            Consumerism,
            Radiation,
            Radical;       
        
        [HideInInspector]
        public Stat[] statdat
        {
            get
            {
                return new Stat[]
                {
                    Health,
                    Moxie,
                    Viscocity,
                    Consumerism,
                    Radiation,
                    Radical
                };
            }
        }

        [System.Serializable]
        public class Stat
        {
            public int value;
            public int cyclesToUpdate;
            public int updateValue;
        }

        public void UpdateStat(int id, int value)
        {
            switch (id) {
                case 0:
                    Health.value += value;
                    break;
                case 1:
                    Moxie.value += value;
                    break;
                case 2:
                    Viscocity.value += value;
                    break;
                case 3:
                    Consumerism.value += value;
                    break;
                case 4:
                    Radiation.value += value;
                    break;
                case 5:
                    Radical.value += value;
                    break;
            }
        }

    }
}
